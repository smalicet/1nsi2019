### Exercice :  
  
On trouve sur le site [https://www.data.gouv.fr/](https://www.data.gouv.fr/fr/datasets/?page=3) des fichiers CSV donnant les prénoms des enfants nés en France par année.  
Voici les listes des prénoms de ceux nés en 2003 et 2004 :  
* lien fichier 2003 : [Prenoms2003.csv](./Prenoms2003.csv)  
* lien fichier 2004 : [Prenoms2004.csv](./Prenoms2004.csv)  

1. Importer ces fichiers CSV via Python puis créer deux tables de dictionnaires comportant ces données : Table_Prenoms2003 et Table_Prenoms2004.  
2. Effectuer la réunion de ces deux tables.  
3. Exporter les données obtenues dans un fichier CSV nommé : Prenoms2003-2004.csv  
4. Donner la liste des 10 prénoms les plus fréquents avec les occurrences pour la période 2003.  
*On proposera un affichage explicite de la forme :  Prénom 2003 n°1 = _PRENOMS_RARES - Occurrence = 16870, comme ci-dessous :*   
![exjonction](exjonction.jpg)
5. Donner la liste des 10 prénoms les plus fréquents avec les occurrences pour la période 2004.  
6. On cherche maintenant à réunir les données en créant une table des prénoms des enfants nés en 2003 ou 2004.  
On fera la somme des nombres pour chaque prénom.  
Créer une table de jointure avec somme des occurrences.  
7. Donner la liste des 10 prénoms les plus fréquents pour la période 2003-2004, d’une part pour les filles, d’autre part pour les garçons.
8. *Bonus :* Faire une étude globale pour les prénoms de 2005 à 2010, de 2010 à 2018 ou pour des
périodes de votre choix.  