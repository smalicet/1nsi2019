# **Les Dictionnaires**


Après les tuples et les listes, les dictionnaires sont
le troisième type de donnée construit au programme.

## 1 - Définition

Un dictionnaire est un ensemble de paires _(clé, valeur)_ (en anglais, _(key, value)_).
A chaque _clé_, le dictionnaire associe donc une _valeur_.

Par exemple :

    >>> d1 = {'x': 2, 'y':4}

Ici, `d` est un dictionnaire, dont les clés sont `'x'` et `'y'`.
La valeur associée à  la clé `'x'` est 2, la valeur associée à la clé `'y'` est 4.

### a - Clés

Tous les objets immutables de Python sont des clés possibles :
+ chaines de caractères;
+ nombres;
+ tuples ne contenant que des objets immutables.

Par contre, les listes, les fonctions ou les dictionnaires ne peuvent pas
être des clés.

### b - Valeurs

Tout objet de Python peut être une valeur associée à une clé d'un dictionnaire.
Par exemple :

    >>> d2 = {(1,3): round, 2: [4, 2], 'hello': {}}

## 2 - Syntaxe

La syntaxe ressemble à celle des listes. Le clés remplacent les indices.

Pour obtenir la valeur associée à  la clé `'x'` dans le dictionnaire `d1`:

    >>> d1['x']
    2

Pour savoir si `'y'` et `'z'` sont des clés du dictionnaire `d1` :

    >>> 'y' in d1
    True
    >>> 'z' in d1
    False

## 3 - Mutabilité

Les dictionnaires sont, comme les listes, des objets mutables et peuvent
être modifiés.
+ Une valeur peut être changée :

        >>> d1['x'] = 0
        >>> d1
        {'x': 0, 'y': 4}

+ Une nouvelle clé et sa valeur peuvent être rajoutées au dictionnaire :

        >>> d1['z'] = -1
        >>> d1
        {'x': 0, 'y': 4, 'z': -1}


## 4 - Itération

Un boucle `for` permet d'itérer sur les clés d'un dictionnaire :

    >>> for k in d1:
           print(k, d1[k])
    x 0
    y 4
    z -1

Mais __attention__, les dictionnaires ne sont par ordonnés, et rien
ne garanti l'ordre dans lequel vous obtenez les clés pendant l'itération.

(Vous pouvez utilisez la fonction `sorted` pour classer les clés.)

## 5 - Les méthodes des dictionnaires

Les dictionnaires possèdent (entre autres) les méthodes bien utiles suivantes :
+ `copy` : pour obtenir une copie;
+ `pop` : efface une clé et renvoie sa valeur;
+ `keys` : renvoie une pseudo-liste des clés;
+ `values` : renvoie une pseudo-liste des valeurs;
+ `items` : renvoie une pseudo-liste des 2-tuples _(clé, valeur)_.

Par exemple :

    >>> d.pop('z')
    -1
    
    >>> list(d.keys())
    ['x', 'y']
    
    >>> list(d.values())
    [0, 4]
    
    >>> list(d.items())
    [('x', 0), ('y', 4)]


## 6 - Références

Comme tout les objets mutables de Python (les liste notamment),
une variable ne contient qu'une référence vers un dictionnaire,
et pas le dictionnaire lui-même. Ainsi :

    >>> d2 = d
    >>> d2.pop('x')
    0
    >>> d
    {'y': 4}

D'oÃ¹ l'utilité de la méthode `copy` du paragraphe précédent.


Ces références sont particulièrement importantes quand on passe
un dictionnaire en argument d'une fonction : le dictionnaire
original sera modifié à l'intérieur de la fonction, et donc
il sera inutile de le renvoyer avec `return`.


Ainsi, si on définit une fonction `f` de la manière suivante :

    def f(dictionnaire):
        dictionnaire['a'] = 7
    
On obtiendra :

    >>> f(d)
    >>> d
    {'y': 4, 'a':7}

