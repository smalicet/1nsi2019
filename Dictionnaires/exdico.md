# Exercices

## EX 1 : Noms des nombres

Définir un dictionnaire `noms` dont les clés sont les nombres entiers compris
entre 0 et 10 et leurs valeurs le nom en français de ces nombres.

Par exemple :

    >>> noms[4]
    quatre


## EX 2 : Enregistrement

Définir une fonction `enregistre` qui pose des questions à  l'utilisateur
pour renvoyer un dictionnaire contenant les clés suivantes : `nom`, `prénom`,
`age`, `lieu_naissance`. On convertira la valeur de la clé `age` en nombre.


## EX 3 : Affichage d'un enregistrement

Définir une fonction `affiche_enregistrement` qui prend en argument un
enregistrement tel que défini dans la question précédente et l'affiche
avec une belle présentation :

    >>> e = enregistre()
    .
    .
    .
    >>> affiche_enregistrement(e)
    Nom :               John
    Prénom :            Doe
    Age :               24
    Lieu de naissance : New York


### e - Liste des clés

Définir une fonction `obtenir_clefs` qui prend en argument un dictionnaire
et renvoie la liste de ses clés.

### f - Liste des paires

Définir une fonction `obtenir_paires` qui prend en argument un dictionnaire
et renvoie la liste de ses paires (2-tuples) _(clé, valeur)_.

### g - Duplicata

Définir une fonction `duplique` qui prend un dictionnaire en argument
et en construit un copie et la renvoie.

### h - Test des valeurs

Définir une fonction `test_valeur_num` qui prend en argument un
dictionnaire et ne renvoie `True` que si toutes les valeurs
du dictionnaires sont des nombres entiers.


On utilisera la fonction `type` pour tester chaque valeur :

    >>> type(7) == int
    True
    

### i - Incrément des valeurs

Définir une fonction `inc` qui prend en argument un dictionnaire
dont toutes les valeurs sont des nombres (à vérifier avec la fonction
précédente) et modifie ce dictionnaire en incrémentant toutes ses valeurs.

    >>> d = {'a': 4, 'b': 7}
    >>> inc(d)
    >>> d
    {'a': 5, 'b': 8}

### j - Seuil

Définir une fonction `seuil` qui prend en argument un dictionnaire
dont toutes les valeurs sont des nombres et renvoie un dictionnaire
similaire mais dont les clés qui ont des valeurs supérieurs à 10
ont été effacées.

    >>> d = {'x': 9, 'y':13}
    >>> seuil(d)
    {'x': 9}
    >>> d
    {'x': 9, 'y':13}

### k - Vecteurs

Dans ce paragraphe, on définira les vecteurs par des dictionnaires
ayant deux clés : 'x' et 'y', dont les valeurs sont les coordonnées
du vecteur.


+ Définir une fonction contructeur, c'est à dire une fonction `vecteur` qui
  prend deux arguments `x` et `y` et renvoie le vecteur ayant ces coordonnées.

        >>> vecteur(3, -4)
        {'x': 3, 'y': -4}

+ Définir une fonction `norme` qui prend en argument un vecteur et
  renvoie sa norme.
  
        >>> u = vecteur(3, -4)
        >>> norme(u)
        5

+ Définir une fonction `somme` qui prend en argument deux vecteurs
  et renvoie leur somme.
  
        >>> u = vecteur(3, -4)
        >>> v = vecteur(1, 2)
        >>> somme(u, v)
        {'x': 4, 'y': -2}

+ Définir une fonction `multiplie` qui prend en argument un vecteur
et un nombre et les multiplie. La fonction ne renvoie rien.

        >>> u = vecteur(3, -4)
        >>> multiplie(-2, u)
        >>> u
        {'x': -6, 'y': 8}