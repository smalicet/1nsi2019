# EX6 du TD1 :

def NbreLignes(fichiertxt):
    '''
    fichiertxt doit être un fichier texte
    la fonction compte et renvoie le nombre de lignes du fichier
    '''
    fichier = open(fichiertxt,'r')
    table = fichier.readlines()
    nbrelignes = len(table)
    fichier.close()
    return nbrelignes

# EX8 du TD1 :

def nombre_occurrence(lettre,texte):
    fichier = open(texte,'r')
    table = fichier.readlines()
    repetitions = 0
    nbrelignes = 0
    nbrecar = 0
    for ligne in table:
        nbrelignes += 1
        for car in ligne:
            nbrecar += 1
            if car == lettre:
                repetitions += 1
    fichier.close()
    return repetitions,nbrelignes,nbrecar

#EX 6 du TD2 :

import csv

def Paramètres(fichier):
    fichier=open("NotesEleves.csv")
    table=list(csv.DictReader(fichier,delimiter=","))
    fichier.close()
    nbrelignes = len(table) + 1
    nbrecolonnes = len(table[0])
    return nbrelignes,nbrecolonnes

def nbrecolonnes(fichier):
    nbre_virgules,b,c = nombre_occurrence(',',fichier)
    nbre_virgules_parligne = nbre_virgules // NbreLignes(fichier)
    return nbre_virgules_parligne + 1
                
                

    
    