# **TD : Organisation des fichiers : les répertoires.**


# 1.	 Qu’est-ce qu’un fichier ?  
Un fichier est un document conservé sur disque (disque dur, clé USB, CD-ROM,  etc.).  
Ce document peut contenir du texte, des images, des sons, de la vidéo, des programmes, représentant chacun un type de fichier différent.  
Le Type d’un fichier est donné par son extension. L’extension est la suite de 3 lettres situées après le point.  
  
Quelques exemples :  
1.	```.txt```, ```.docx```, ```.odt``` sont les extensions pour les fichiers texte, Word et Libreoffice
2.	```.xlsx``` est l’extension pour les fichiers Excel
3.	```.jpg``` et ```.png``` sont les extensions pour les fichiers …
4.	```.mp3``` et ```.m4a``` sont les extensions pour les fichiers …
5.	```.mp4``` et ```.mov``` sont les extensions pour les fichiers …  
  

# 2.	Qu’est-ce qu’un répertoire ?
On pourrait mettre tous les fichiers sur le bureau de son ordinateur mais il serait difficile de retrouver rapidement ce que l’on cherche si le nombre de fichiers est important.  
On a donc besoin de classer les fichiers de telle sorte qu’on puisse les retrouver facilement.  
**Un répertoire** (appelé aussi **dossier**) est tout simplement un endroit de rangement de nos fichiers informatiques.  
On pourra ainsi créer un répertoire  ```Banques``` et encore un répertoire ```Assurances``` pour ranger les fichiers correspondant à ces catégories.  
En informatique, un répertoire ou un dossier est une liste de descriptions de fichiers.  
Du point de vue du système de fichiers, il est traité comme un fichier dont le contenu est la liste des fichiers référencés.  
Un répertoire a donc les mêmes types de propriétés qu'un fichier comme le nom, la taille, la date, les droits d'accès et les divers autres attributs. 

# 3.	Qu’est-ce qu’une arborescence ?
Pour comprendre l’organisation d’un disque dur, on peut imaginer une armoire contenant des tiroirs contenant des chemises (ou pochettes) qui, à leur tour, contiennent des sous-chemises, qui, à leur tour, peuvent contenir des sous-chemises et ainsi de suite.  
Il faut imaginer que chacun de ces contenants peut également contenir des feuilles.  
En informatique, on retrouve la même organisation en arborescence :  
•	le disque dur de l’ordinateur c’est l’armoire,
•	Les répertoires sont les tiroirs et les chemises,
•	les sous-répertoires  sont les sous-chemises,
•	Les  fichiers informatiques (documents texte, image, vidéo…) sont les feuilles.  
  
  
![armoire](./fig/armoire.jpg)  

Par exemple, physiquement, si je veux retrouver ma facture de Gaz de 2012,  
j’ouvrirais l’armoire,  
puis le tiroir document,  
puis la chemise EDF,Gaz,  
puis je prendrai la feuille Gaz 2012  

D’un point de vue informatique, 
je partirai du disque dur,  
puis repertoire Documents,  
puis sous-repertoire EDF-Gaz  
puis fichier Gaz 2012  

  
L’arborescence classique sous Unix ressemble à cela :  
![arbo](./fig/arbo.jpg)  
Notons que le répertoire racine est particulier puisqu’il est le seul à n’être contenu dans aucun répertoire.
 
On appelle d’ailleurs répertoire parent (ou parfois répertoire supérieur) le répertoire dans lequel est directement contenu notre élément.  

*Exemple :*  
Ici, le répertoire parent du répertoire `projet 1` est  ….  
le répertoire parent du fichier `d1` est …
  
  
# 4.	Qu’est-ce qu’un chemin (appelé aussi « path ») ?
C’est un enchainement de répertoires imbriqués, avec un fichier ou un répertoire à la fin, séparés par le caractère / indiquant comme son nom l’indique le chemin à suivre pour accéder à l’élément final.  
En reprenant l’exemple, si je me trouve dans le répertoire `Users` et que je veux accéder au fichier `p1` je dois suivre le chemin `Dupond / projet1 / p1`
On appelle cela un **chemin relatif** (sous-entendu au dossier courant). 
Parfois, il arrive de parler de **chemins absolus**, c’est-à-dire qui ne prennent pas en compte notre position dans l’arborescence.  
En reprenant l’exemple précédent le chemin absolu est  `/ Users / Dupond / projet1 / p1`

# 5.	Commandes de manipulation de répertoires.

### 1) Visualisation du contenu : `ls`  
`ls` donne le contenu du répertoire où l'on se trouve, dans l'ordre suivant : symboles, chiffres, majuscules, minuscules.  
Les fichiers apparaissent sous leur nom, les sous-répertoires ont leur nom suivi d'un slash (par exemple, machin/).  
Les noms de fichiers suivis d'une étoile sont des exécutables.  
Il est possible d'obtenir des niveaux de détails plus importants grâce aux options suivantes :  
* ls -s liste les fichiers ainsi que leurs tailles en kilo-octets.
* ls -l liste l'ensemble des informations inhérentes à chacun des fichiers.
* ls -a liste tous les fichiers (même ceux commençant par  .  , c’est-à-dire les fichiers cachés).  
  

*Application :*  
Testez la commande `ls` en affichant, depuis votre répertoire personnel initial, la liste de tous vos fichiers et sous-répertoires :  
1. sous un format condensé 
2. sous un format long (donnant le propriétaire, les permissions, la taille, ...) 
3. en affichant les fichiers cachés (dont le nom commence par un point)
  

### 2) 	Afficher le chemin complet du répertoire courant : pwd
Application : Utilisez la commande pwd et observez ce qui est obtenu
### 3)	Créer un répertoire : mkdir
Application : Visualisez le contenu de votre dossier puis créer un dossier appelé test et revisualiser le contenu du dossier pour confirmer sa présence.
### 4)	Supprimer un répertoire : rmdir
Application : Supprimez le dossier test crée au 5.2
### 5)	Se déplacer dans les dossiers : cd
Application : Créez un répertoire intitulé test puis placez-vous dedans 2. Créez un fichier texte.txt grâve à la commande touch texte.txt 3. Visualisez le contenu du dossier test.
### 6)	Le répertoire parent est noté :  ..
Attention de ne pas confondre avec  .  qui représente le répertoire courant (on verra plus tard l’utilité)
Application : Sortez du répertoire test et retournez dans le répertoire initial. Vérifiez en affichant le répertoir courant.
### 7)	Déplacer un fichier : mv
Il y a ici deux arguments : le premier est le fichier déplacé et le deuxième le chemin du lieu où le copier.
Application : 
Placez-vous dans le dossier test  puis exécutez  « $ mv test  . » Que s’est-il passé ?
Placez-vous dans le dossier test  puis exécutez  « $ mv test nouveau » Que s’est-il passé ?
La commande déplacer sert également à …
### 8)	Afficher le chemin complet du répertoire courant : pwd
*Application :* Utilisez la commande pwd et observez ce qui est obtenu

# **Exercices :**  

## Exercice 1 :
Où que vous soyez, quel est l'effet de la commande cd sans paramètre ?  

## Exercice 2 : 
1. Créez un répertoire `system` sous votre répertoire de travail, puis un répertoire `tp1` sous `system `
2. Effacez le répertoire `system` avec la commande `rmdir`. Que constatez-vous ? 
3. Après avoir effacé les répertoires `tp1` et `system`, créez à l'aide d'une seule commande les répertoires `system`, `system/tp1`, `system/tp2` 
4. Renommez le répertoire `system` en `test` 
5. Effacez à l'aide d'une seule commande les répertoires `test/tp1` et `test/tp2`  
 

## Exercice 3 :
1. Combien y a-t-il de noms de répertoires dans la racine ? 
2. Donnez un exemple de nom de fichier se trouvant dans votre répertoire personnel :  
a) par un chemin relatif    
b) par un chemin absolu.  


## Exercice 4 :  
Dans votre répertoire d'accueil (`/home/user` par exemple), créez l'arborescence suivante, en n'utilisant que des chemins relatifs :  
![ex4](./fig/ex4.jpg)  
Puis vérifiez
