Jusqu'à présent nous avons rencontré différents paradigmes de programmation (des manières de programmer) :

* __la programmation impérative__ : dans laquelle des séquences d'instructions s'enchainent
* __la programmation fonctionnelle__ : basée sur les fonctions
* __la programmation logique__ : basée sur des opérateurs et des régles de logique

Python est un langage multi-paradigmes car on a pu utilisé ces derniers en les combinant.
Un autre paradigme couramment utilisé est la __programmation évenementielle__ que nous allons présenté en utilisant le langage Javascript.


# Evenements

Javascript est très souvent utilisé sur la machine du client web (la votre, _à distinguer de celle du serveur_) pour rendre la page interactive en réponse aux actions de l'utilisateur :

* un clic sur un élément
* un survol d'un élément
* un appui sur une touche du clavier etc...

> 📢 A chaque __évenement__ est associé un __gestionnaire d'événement__ : en général une fonction qui sera exécutée lorsque l'événement sera déclenché.

Regardons un exemple simple : suivez lien pour accéder à JS Fiddle (un service en ligne permettant de tester des extraits de code HTML, CSS et JS)

https://jsfiddle.net/gjquxr68/

Remarques :

- le code HTML se limite au contenu de l'élément body : ici un élément `button`
- les liens vers les fichiers css et javascript sont gérés par l'interface : ils n'apparaissent pas

Intéressons nous au code Javascript :

```javascript
let btn = document.querySelector('button');  
// Permet de récupérer l'élément button présent sur la page 

btn.addEventListener('click', popup);  
// met en place une fonction à appeler à chaque clic sur l'élément 

function popup(){   
	window.alert('vous avez cliqué sur le bouton ! ')
}
```

La mise en place d'évenement est basée sur la stratégie suivante :

* on affecte à une variable un élément de la page
* on abonne une fonction à un élement pour un évenement donné
* La fonction sera exécutée lorsque l'événement se produira sur l'élément ciblé.


La contenu de la fonction est bien évidemment adaptable : regardez le deuxième exemple :


https://jsfiddle.net/k9mrjy32/


```javascript
function modif_div(){
	let div = document.querySelector('div');
    div.style.backgroundColor = 'blue';
    /* remarque les propriétés composées comme background-color
    s'écrivent comme backgroundColor en javascript
    on parle de syntaxe Camel case*/
}
```

La fonction `modif_div` permet ici d'agir sur les propriétés de style de l'élément `div`

Il existe bien d'autres événements comme :

* `dblclick` : un double-clic
* `mouseover` : un survol
* `keypress` : un appui sur une touche
* `load` : un chargement (celui de la page par exemple)
* etc...


# Séparation des codes HTML, CSS et Javascript.

Il est préférable de faire une sépration claire des codes HTML, CSS et Javascript.
Comme on l'a précédemment vu : les scripts au langage JAvascrpt auront une extension de type `.js` .
Il suffit alors d'appeler le script juste avant la balise body fermante comme dans l'exemple ci-après :

```html
<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8"/>
    <title>exemple</title>
</head>
<body>
    <button>Cliquez-ici</button>
    <script src="script_exemple.js"></script>
</body>
</html>
```

# Applications :

# Exercice 1 :  Bonnes pratiques &#x1F3C6;

__1.__ Récupérer tout d'abord l'archive [`bonnes_pratiques_js.zip`](./bonnes_pratiques_js.zip) et décompressez-la. A l'aide d'un éditeur de votre choix visualiser le code html de `index1.html` et afficher-la dans le même temps avec le navigateur de votre choix pour comprendre le code associé.

__a)__ Dissocier le CSS du reste du code en créant un fichier séparé et en faisant le lien avec la page html.

__b)__ Dissocier le code JS de la même manière.

__c)__ Il reste encore des "traces" de javascript dans le code html : il s'agit de la propiété `onclick`. A l'heure actuelle il est recommandé d'éviter d'utiliser cette propriété.
Modifiez les codes html et javascript afin de respecter les bonnes pratiques présentées en cours pour la gestion d'événements.

# Exercice 2 :  Une page qui a du style &#x1F3C6;  &#x1F3C6;

__1.__ Soit une page HTML contenant un élément `p` dont l'`id` est `p1` et un bouton.

```html
....
<p id="p1">un paragraphe</p>
<button>Modifier le paragraphe</button>
...
```

En respectant les bonnes pratiques vues dans l'exercice 1, mettez en place un gestionnaire d'événément `modif_style` associé au clic sur le bouton qui aura pour effet de modifier quelques propriétés de style de la page :

* la couleur du texte deviendra bleue
* le paragraphe sera encadré par un trait continu noir de largeur 2px.
* l'élément body aura une couleur d'arrière-plan cyan.

__2.__

__a-__ Supprimez le bouton, maintenant les changements précédents devront s'effectuer lors du survol du paragraphe.

__b-__ Pour aller plus loin vous pouvez détécter la sortie de la zone de survol avec l'événement `mouseout` pour réinitialiser les propiétés de style.




