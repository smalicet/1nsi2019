Les formulaires sont essentiels dans les sites web car ils permettent de collecter des données sur l'utilisateur et donc de personnaliser les pages envoyées par le serveur par la suite. 

![Page web du formulaire](./fig/formulaire.jpg)  

Nous allons dans un premier temps concevoir un formulaire simple puis nous simulerons la relation client-serveur en envoyant les données vers un serveur créé sur un poste voisin et analyserons les données transmises liées aux requêtes du protocole HTTP

Récupérez l'archive [`formulaire.zip`](./formulaire.zip) et décompressez_la. 
 
* Vous avez un fichier HTML pré-rempli `formulaire.html`. Pour construire votre formulaire, vous devrez remplacer les zones de commentaires par les balises utiles. 

* Vous avez un fichier CSS lié au fichier HTML du formulaire. Il est rempli, vous pouvez le modifier pour personnaliser votre travail si vous le souhaitez lorsque l'ensemble du travail de la séance sera accompli.

* Vous utiliserez les éléments html présentés ci-après pour completer le fichier `formulaire.html` afin d'obtenir un formulaire similaire à celui présenté dans la __figure 1__.

* Dans la div qui porte l'id `titre`, vous mettrez "Premier Formulaire" suivi de votre nom. (Cela permettra de reconnaitre votre formulaire.)  

* Avant de m'envoyer votre fichier formulaire.html, vous le renommerez sous la forme formule_nom.html pour que je puisse être plus efficace dans mes corrections.
  

# Conception du formulaire : élements html.

## Element `<form>`  

Il représente une section du document  qui contient des contrôles interactifs permettant à un utilisateur d'envoyer des données à un serveur web.

| Attributs | Descriptions |    
|:-----------:|:------------------------:|     
| `action`  | URL vers laquelle les informations soumises au formulaire sont envoyées|    
| `method` | La méthode HTTP utilisée pour envoyer des données au serveur |  

```html
<!-- Formulaire simple qui enverra une requête GET -->
<form action="" method="get">
  <label for="GET-name">Nom :</label>
  <input id="GET-name" type="text" name="name"/>
  <input type="submit" value="Enregistrer"/>
</form>
```

## Elements `<input>` et `<label>`

L' élément `<input>` a pour but de recueillir les informations d'une entrée effectuée par l'utilisateur. Il est de nature multiple : un champ de texte, un bouton radio, une case à cocher....  
Il est généralement associé à une étiquette qui permet de le décrire : l'élément `<label>`.

### Les champs textes
#### Champ texte basique

* _Exemple_:    ![champ texte](./fig/input.jpg)

```html 
    <label for = "prenom"> Prénom: </label>  
    <input id="prenom" name ="prenom" type="text" value="prénom"/>  
```  

* pour l'élément `<input>` :

| Attributs | Description |     
|:-----------:|:------------------------:|      
| `type = "text" `| Définit un type input de champ texte. |      
| `name`  | Nom du champ rattaché à la donnée envoyée |    
|`id` | identifiant lié formellement à l'attribut `for` de l'élément `label` |     
| `value` | valeur du champ |    


* pour l'élément `<label>` :

| Attributs | Description |     
|:-----------:|:------------------------:|         
|`for` | identifiant lié formellement à l'attribut `id` de l'élément `input` |     

#### mot de passe

* _Exemple_:    ![mot de passe](./fig/pwd.jpg)

```html 
    <input id="pwd" name ="motdepasse" type="password"/>  
```  


#### email

* _exemple de code associé_ :

```html 
    <input type="email" id="mail" name ="adressemail" />  
```  
 
### Créer un bouton radio

* _Exemple_ :  ![bouton radio](./fig/radio.jpg)

```html 
    <label for = "pere"> Père :</label>  
    <input id="pere" name ="parent" type="radio" value="pere"/>   
    <label for = "mere"> Mère :</label>  
    <input id="mere" name ="parent" type="radio" value="mere" checked="checked"/>   
```

* pour l'élément `<input>` :

| Attributs | Description |     
|:-----------:|:------------------------:|      
| `type = "radio" `| Définit un bouton radio. |      
| `name`  | Tous les boutons radio d'un même groupe doivent avoir le mêmme attribut name |  
| `checked = "checked"`| Permet de sélectionner un des boutons radio du groupe par défaut |   

## Elements `<select>` et `<option>` 

L'élement `<select>` permet de créer une liste de choix le plus souvent déroulante à effectuer parmi plusieurs éléments `<option>` 

* Exemple :  ![liste déroulante](./fig/liste_deroulante.jpg)

```html 
    <label for = "annee"> Année </label>  
    <select id = "annee" name ="annee" value="2019">
        <option value='2019'>2019</option>
        <option value='2020'>2020</option>
    </select>   
```  

## Validation du formulaire

On rencontrera nécessairement un élément `<input>` de type __submit__ à la fin du formulaire qui permettra la validation des données.

```html 
<input id="bouton" type="submit" value="Valider"/>
```
