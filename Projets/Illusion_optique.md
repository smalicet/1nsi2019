# Dessinons avec le module turtle  

L'objet du projet est la manipulation d'un nouveau module : turtle (pensez à l'installer sur Thonny par exemple)
![tuto1](./fig/tuto1.png)  
![tuto2](./fig/tuto2.png)

D'autre part, il vous sera utile de bien réfléchir en amont aux différentes opérations
élémentaires qui vous feront gagner un temps précieux.  

[Voici la documentation du module turtle de Python](https://docs.python.org/fr/3/library/turtle.html)  

[Voici quelques exercices pour vous familiariser avec le module turtle](https://parcours.algorea.org/contents/4707-4702-1352246428241737349-314613032161178344-310572474623192846/)  
  
[Voici quelques exemples d'instructions de coloriage](https://zestedesavoir.com/tutoriels/944/a-la-decouverte-de-turtle/colorier/#4-tp-atelier-coloriage)

## Travail à réaliser :  

Voici une illusion d’optique qu’il s’agit de reproduire avec la tortue :  
![illusion](./fig/illusion_optique.png)  

  
*Le travail peut se faire seul ou en binôme.*

## Bon Travail
