#---------------------------------------------
#  Importation des modules
#---------------------------------------------


#----------------------------------------------
# Fonction création jeu de cartes 
#----------------------------------------------

def jeu_de_cartes() :
    """Fonction qui retourne une liste comportant un jeu de 52 cartes."""
    return ["As ♠", "2 ♠", "3 ♠", "4 ♠"
                    , "5 ♠", "6 ♠", "7 ♠", "8 ♠"
                    , "9 ♠", "10 ♠", "Valet ♠"
                    , "Dame ♠", "Roi ♠", "As ♣", "2 ♣"
                    , "3 ♣", "4 ♣", "5 ♣", "6 ♣"
                    , "7 ♣", "8 ♣", "9 ♣", "10 ♣"
                    , "Valet ♣", "Dame ♣", "Roi ♣"
                    , "As ♥", "2 ♥", "3 ♥", "4 ♥"
                    , "5 ♥", "6 ♥", "7 ♥", "8 ♥"
                    , "9 ♥", "10 ♥", "Valet ♥"
                    , "Dame ♥", "Roi ♥", "As ♦"
                    , "2 ♦", "3 ♦", "4 ♦"
                    , "5 ♦", "6 ♦", "7 ♦", "8 ♦"
                    , "9 ♦", "10 ♦", "Valet ♦"
                    , "Dame ♦", "Roi ♦"]

#----------------------------------------------
# Fonctions d'affichage
#----------------------------------------------

def afficher_carte(jeu1, jeu2) :
    pass


def afficher_nombres_cartes(jeu1, jeu2, nom1, nom2) :
    pass


def afficher_vainqueur_tour(gagnant, nom1, nom2) :
    pass
 
 
def afficher_vainqueur_jeu(gagnant, jeu1, jeu2, nom1, nom2) :
    pass


#----------------------------------------------
# Fonctions relatives aux cartes des joueurs
#-----------------------------------------------

def valeur_carte(carte) :
    pass


def comparer_carte(carte_joueur1, carte_joueur2) :
    pass
   
   
#----------------------------------------------
# Fonctions relatives aux jeux des joueurs
#-----------------------------------------------

def distribution_jeu(jeu_complet, nombre_cartes) :
    pass


def ordonner_jeu(jeu_vainqueur, jeu_perdant) :
    pass


def gestion_bataille(jeu_joueur1, jeu_joueur2, gagnant_bataille) :
    pass


def gestion_tour(gagnant, jeu_joueur1, jeu_joueur2) :
    pass

  
  
#---------------------------------------------
# Fonction principale
#---------------------------------------------
   

def jeu() :
    pass