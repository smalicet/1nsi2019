# **1NSI**

*Cours de NSI pour les premières*  

  
# ***PROCHAIN RDV MARDI 26 MAI 15h00 SUR DISCORD***

Ici vous trouverez les dernières actualités avec notamment les travaux à réaliser.<br>
Cette page sera mise à jour à chaque fin de visioconférence ou de période de devoirs.<br>
**Je vous conseille de la mettre dans les favoris de votre ordinateur afin de la retrouver facilement.**  
Nous allons profiter de cette période pour avancer plus rapidement sur le projet, 
toutefois chaque semaine je vous mettrai un TD ou cours en format markdown (fichier 
finissant par .md) que vous trouverez dans le dossier javascript.  
Je ferai de temps à autre des réunions sur le site du [**CNED : ma classe à la maison**](https://lycee.cned.fr/).
Je suis sur le réseau social [**Discord**](https://discordapp.com/) avec pas mal d'autres élèves (principalement pour les premières et terminales) : Vous pouvez rejoindre ce salon intitulé NSI pour rentrer en contact avec moi via ca lien : [https://discord.gg/hu6anVU](https://discord.gg/hu6anVU)

### **Ma classe à la maison :**
Allez sur le site du CNED : [https://lycee.cned.fr](https://lycee.cned.fr/)  
Créez vous-y un compte qui vous servira peut-être avec d'autres enseignants également.  
Connectez-vous à la date et heure indiquée par moi-même en cliquant sur le lien que je mettrai ici :  
**Prochain rendez-vous :** Jeudi 9 Avril 15h00 
**Lien de connection :** [classe virtuelle](https://eu.bbcollab.com/guest/01b3df29bd10494fbedfd457ce9c6d30)

# semaine du 16 au 22 mars :
Travail à réaliser :
1. Pour ceux qui n'ont pas encore fini le TD [javascript.md](https://framagit.org/smalicet/1nsi/-/blob/master/Javascript/javascript.md) faire au moins jusqu'à l'exercice 5<br>
2. Faire le TD [évènementiel.md](https://framagit.org/smalicet/1nsi/-/blob/master/Javascript/evenementiel.md)
3. Avancer sur votre projet de groupe

  
# semaine du 23 au 29 mars :

Nous allons cette semaine, voir comment on peut concevoir un formulaire.    
La réalisation de ce TD de façon sérieuse vous permettra d'insérer un formulaire dans votre site Web .  
*Ce TD sera évalué et vous aurez une note pour la réalisation de celui-ci*  
La semaine prochaine, nous verrons comment récupérer les données d'un formulaire.

Travail à réaliser :
1. Faire le TD [formulaire.md](https://framagit.org/smalicet/1nsi/-/blob/master/Javascript/formulaire.md)  
2. M'envoyer votre fichier formulaire.html fini en email à l'adresse : sebastien.malicet@ac-lille.fr (c'est ma boite mail de prof)
3. Avancer sur votre projet de groupe
  

# semaine du 30 au 4 avril :

Rectificatif : la partie utilisation du formulaire sera faite ultérieurement ensemble en classe car je la trouve 
difficile à faire à distance (nécessite des installations et paramétrages trop complexes 
pour que les fassiez de chez vous, sans parler de la partie matérielle qui peut nous contraindre)  



Travail à réaliser :
1. Lire / Comprendre / Apprendre le [fonctionnement du HHTP](https://framagit.org/smalicet/1nsi/-/blob/master/Http/http.md)
2. Lire / Comprendre le [cours sur les tuples en python](https://framagit.org/smalicet/1nsi/-/blob/master/Tuples/tuples.md)
3. Faire les [exercices sur les tuples](https://framagit.org/smalicet/1nsi/-/blob/master/Tuples/extuples.md)    Voici un exemple de [corrigé](https://framagit.org/smalicet/1nsi/-/blob/master/Tuples/cor_tuples.py) (c.f. travail de vendredi)
4. Prendre connaissance du [second projet](https://framagit.org/smalicet/1nsi/-/blob/master/Projets/Illusion_optique.md) (individuel voire binôme celui-ci)
5. Avancer sur vos projets (l'inviduel quand vous êtes seul, celui du site lorsque vous arrivez à vous rencontrer en ligne)  
  
  

*Prochain RDV : Date et horaire à confirmer ensemble*  

# semaine du 6 au 10 avril :
  
On va travailler sur un notebook cette semaine.  
Voici les 3 méthodes pour lire un jupyter notebook :  
  
**1. La meilleure méthode : Télécharger jupyter**  
On va passer par thonny : (**Tools > Manage packages**) :  
![tuto1](./Dictionnaires/tuto1.png)  
Ensuite exécutez le shell de Thonny (**Tools > Open System Shell**) et tappez *jupyter notebook* :
![tuto2](./Dictionnaires/tuto2.png) 
Télécharger le notebook [ICI](https://framagit.org/smalicet/1nsi/-/blob/master/Dictionnaires/dictionnaires.ipynb)(bouton en haut à droite Download) et mettez le dans le dossier dans lequel vous avez exécuté le jupyter notebook : 
![tuto3](./Dictionnaires/tuto3.png) 
Voilà, vous pouvez travailler. *N'oubliez pas de le sauvegarder et de le renommer.*

**2. La méthode la plus simple : Avec Binder**  
Binder crée un ordinateur virtuel qui exécute le notebook et affiche le résultat sur votre navigateur.  
Vous pouvez y accéder directement en cliquant ici : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2F1nsi/master?filepath=Dictionnaires%2Fdictionnaires.ipynb
)  
*Attention il faut parfois attendre 2-3 minutes pour que l'ordi virtuel soit créé*  

**3. La méthode déjà utilisée en classe : Avec Google Colab**  
Télécharger le notebook [ICI](https://framagit.org/smalicet/1nsi/-/blob/master/Dictionnaires/dictionnaires.ipynb)(bouton en haut à droite Download)  
Allez sur [google colab](https://colab.research.google.com/), connectez vous à votre drive, chargez le notebook et travaillez.



**Travail à réaliser :**  
1. TD sur le fonctionnement des dictionnaires.  
N'oubliez pas d'enregistrer votre travail *dico_nom_prenom* pour me le déposer dans le casier de l'ENT : ![tuto](./tutojupi2.png)  

2. Avancer sur vos projets (l'inviduel quand vous êtes seul, celui du site lorsque vous arrivez à vous rencontrer en ligne)  
  
**Travail supplémentaire :**    
1. Pour ceux qui le désirent, voici un petit TD sur le manipulation du module matplotlib :  
Lien vers le notebook à télécharger : [ICI](https://framagit.org/smalicet/1nsi/-/blob/master/Graphiques/matplot.ipynb)   ou lien direct vers Binder : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2F1nsi/master?filepath=Graphiques%2Fmatplot.ipynb)  
2. Voici un autre projet individuel : [le jeu de bataille](https://framagit.org/smalicet/1nsi/-/blob/master/Projets/jeu_bataille.md) (projet réalisé par l'équipe Margueritte de Flandres de Lille)
  

# semaine du 27 au 30 avril :
  
Cette semaine, on va travailler sur les fichiers.  
On va dans un premier temps voir comment les manipuler à l'aide du terminal. Au programme cette manipulation est à faire sous UNIX (environnement Linux).  
Si comme moi vous utilisez windows, installez simplement [cygwin](https://cygwin.com/install.html) et executez le.  
Pour les élèves sous Linux lancez UNIX.  
Voici le cours accompagné d'exercices de manipulation des commandes de base : [TD manipulation de fichiers](https://framagit.org/smalicet/1nsi/-/blob/master/Gestion%20de%20fichiers/organisation%20des%20fichiers.md)  
*Ces commandes sont à maitrisées et à apprendre car elles peuvent être demandées en interrogation.*


# semaine du 4 au 7 mai :

Pour les absents lors de la classe virtuelle de lundi, voici le [TD sur la gestion des fichiers en python](https://framagit.org/smalicet/1nsi/-/blob/master/Gestion%20de%20fichiers/gestion_fichiers.md)  
Pour les absents lors de la classe virtuelle de jeudi, voici le support de cours : [TD sur les fichiers CSV](https://framagit.org/smalicet/1nsi/-/blob/master/Gestion%20de%20fichiers/fichiers_csv.pdf)  
  
# semaine du 11 au 15 mai :


Finir le jeu [Terminus](http://luffah.xyz/bidules/Terminus/) (personnellement j'ai mis environ 1h00 pour le faire, donc c'est normal si cela vous prend 2 ou 3 heures).  
Je vous interrogerai en MP pour voir si vous l'avez fini.  
  
Correction des 2 TD de la semaine dernière faite en classe virtuelle à 16h lundi 11 mai :  
Notamment les exercices 6 et 8 du [TD1](https://framagit.org/smalicet/1nsi/-/blob/master/Gestion%20de%20fichiers/gestion_fichiers.md)  
et l exercice 6 du [TD2 ](https://framagit.org/smalicet/1nsi/-/blob/master/Gestion%20de%20fichiers/fichiers_csv.pdf)  
Télécharger le fichier python de la correction [ICI](https://framagit.org/smalicet/1nsi/-/blob/master/Gestion%20de%20fichiers/correctiondu12mai.py)  

Vendredi, je vous présenterai un petit TD sur les traitements de tables récupérées : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2F1nsi/master?filepath=CSV%2Ftraitement_tables_csv.ipynb)


# semaine du 18 au 20 mai :

Classe virtuelle mardi pour la correction du TD sur les traitements de tables récupérées  

Question 3 :  
```python
import csv

fichier = open('membre.csv','r',encoding='utf-8')
membre = list(csv.DictReader(fichier))
fichier.close()

fichier = open('pret.csv','r',encoding='utf-8')
pret = list(csv.DictReader(fichier))
fichier.close()

fichier = open('livre.csv','r',encoding='utf-8')
livre = list(csv.DictReader(fichier))
fichier.close()

def union(ligne1,ligne2):
    return {'prénom':ligne1['prénom'],'idm':ligne1['idm'],'idl':ligne2['idl']}

jointure = []
for ligne1 in membre:
    for ligne2 in pret:
        if ligne1['idm'] == ligne2['idm']:
            jointure.append(union(ligne1,ligne2))

print(jointure)
```
Question 4 :  
```python
import csv

fichier = open('membre.csv','r',encoding='utf-8')
membre = list(csv.DictReader(fichier))
fichier.close()

fichier = open('pret.csv','r',encoding='utf-8')
pret = list(csv.DictReader(fichier))
fichier.close()

fichier = open('livre.csv','r',encoding='utf-8')
livre = list(csv.DictReader(fichier))
fichier.close()

def union(ligne1,ligne2,ligne3):
    return {'prénom':ligne1['prénom'],'idm':ligne1['idm'],'idl':ligne2['idl'],'titre':ligne3['titre']}

jointure = []
for ligne1 in membre:
    for ligne2 in pret:
        if ligne1['idm'] == ligne2['idm']:
            for ligne3 in livre:
                if ligne2['idl'] == ligne3['idl']:
                    jointure.append(union(ligne1,ligne2,ligne3))

print(jointure)

fichier = open('exercice.csv','w')
joint = csv.DictWriter(fichier,['prénom','idm','idl','titre'])
joint.writerows(jointure)
fichier.close()
```
  
  
[Faire l'exercice sur la jonction de tables](https://framagit.org/smalicet/1nsi/-/blob/master/CSV/exjonction.md)  

# Semaine du 26/05 : 
  
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2F1nsi/master?filepath=algo%2FDichotomie.ipynb)  
  
  
# Semaine du 02/06 :  

## Les algorithmes glouton.
Voici un exemple d'algorithme du rendu de monnaie  

```python
# on importe les deux modules qui nous permettent de tester la vitesse des algorithmes sur des nombres aléatoires
from random import *
from timeit import timeit

L =[1,3,6,12,24,30]

def rendu_monnaie(somme, liste):
    liste.sort(reverse=True)
    nbpièce=0
    while somme !=0:
        for i in range(len(liste)):
            while somme >= liste[i]:
                somme -= liste[i]
                nbpièce+=1
    return nbpièce


def rendu_monnaie_opti(somme, liste):
    liste.sort(reverse=True)
    nb=0
    for valeur in liste:
        nb+=somme//valeur
        somme=somme%valeur
    return nb

#pour tester la vitesse des fonctions:
t1 = timeit(lambda:rendu_monnaie(randint(1,10000),L),number=10000)
print("avec l'algo non opti on met",t1,"secondes pour 10 000 nombres")
t2 = timeit(lambda:rendu_monnaie_opti(randint(1,10000),L),number=10000)
print("avec l'algo optimisé on met",t2,"secondes pour 10 000 nombres")
```

[Voici un lien vers un cours présentant les algorithmes glouton](https://pixees.fr/informatiquelycee/n_site/nsi_prem_glouton_algo.html)  
  
Synthèse faite par vos camarades :  


## Mardi 9 juin :  

[Travail sur les système d'exploitation](https://framagit.org/smalicet/1nsi/-/blob/master/Syst%C3%A8me%20d'exploitation/os_intro.md)  

## Mercredi 10 juin : 
Travail sur les projets "facultatifs" donnés lors du confinement : [Illusion d'optique](https://framagit.org/smalicet/1nsi/-/blob/master/Projets/Illusion_optique.md)  

## Mardi et Mercredi 16-17 juin : 
**Travail sur la méthode des _knn_** :  
- [Cours](https://framagit.org/smalicet/1nsi/-/blob/master/knn/cours_knn.md) 
- [TD sans ordi](https://framagit.org/smalicet/1nsi/-/blob/master/knn/exo-td-knn.md)
- **TP : Le choixpeau de Poudlard :**  
*2 méthodes :*  
En direct : [Harry Potter](https://framagit.org/smalicet/1nsi/-/tree/master/knn/harry) Download This directory en .zip > Extraire sur votre ordinateur > Lancer le jupyter notebook  
Avec Binder : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fsmalicet%2F1nsi/master?filepath=.%2Fknn%2Fharry%2Fharry.ipynb)  
Voici son [corrigé](https://framagit.org/smalicet/1nsi/-/blob/master/knn/harry/harry_cor.pdf)