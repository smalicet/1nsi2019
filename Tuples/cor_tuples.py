import doctest



#EXERCICE 1 :


def somme(t):
    '''
    @param t : tuple de nombres
    @return : renvoie un nombre qui est la somme des nombres composant le tuple
    >>> inverse(('a', 2, 3))
    (3, 2, 'a')
    '''
    s = 0
    for i in t:
        assert type(i) in (int,float), "les éléments du tuple doivent être des nombres"
        print(i)
        s += i
    return s


def inverse(t):
    '''
    @param t : tuple 
    @return : renvoie renvoie un tuple qui a ses éléments dans le sens inverse
    >>> inverse(('a', 2, 3))
    (3, 2, 'a')
    '''
    assert t != (), "le tuple en paramètre ne doit pas être vide"
    res = ()
    n = len(t)
    for i in range(n-1,-1,-1):    #range(n-1,-1,-1)  = [n-1,n-2,n-3,...,0]
        res = res + (t[i],)
    return res

def is_right_angle(t):
    '''
    @param t : tuple contenant trois longueurs
    @return : Booléen True si angle droit, False sinon
    >>> is_right_angle((6, 8, 10))
    True
    '''
    assert len(t) == 3, "le tuple doit contenir 3 éléments"
    a,b,c = t
    if a**2 + b**2 == c**2 or a**2 + c**2 == b**2 or c**2 + b**2 == a**2:
        return True
    else:
        return False
    
def infos_notes(t):
    '''
    @param t : tuple contenant trois longueurs
    @return : Booléen True si angle droit, False sinon
    >>> infos_notes([15, 12, 10, 8, 17, 11])
    (12.2, 8, 17)
    '''
    mini = t[0]
    maxi = t[0]
    somme = t[0]
    n = len(t)
    for i in range(1,n,1):
        if t[i] < mini:
            mini = t[i]
        elif t[i] > maxi:
            maxi = t[i]
        somme += t[i]
    moyenne = round(somme / n,1)
    return (moyenne, mini, maxi)



#Exercice 2 :

def milieu(A,B):
    '''
    @param t : A et B sont deux tuples de 2 nombres
    @return : tuple des coordonnées du milieu de [AB]
    >>> A = (3, 12)
    >>> B = (-4, 5)
    >>> milieu (A, B)
    (-0.5, 8.5)
    '''
    x = (A[0]+B[0]) / 2
    y = (A[1]+B[1]) / 2
    return x,y

def point_sup_droit(liste):
    '''
    @param t : A et B sont deux tuples de 2 nombres
    @return : tuple des coordonnées du milieu de [AB]
    >>> (0,1) < (1,2) 
    True
    >>> (1,1) < (1,2) 
    True
    '''
    maxi = liste[0]
    for elt in liste :
        if elt > maxi:
            maxi = elt
    return maxi


#Exercice 3 :


def rgb_to_hex(triplet):
    '''
    @param triplet : tuple contenant 3 nombres entiers entre 0 et 255
    @return list : couleur en héxadécimale
    
    >>> rgb_to_hex((255, 0, 255))
    '#ff00ff'
    
    >>> rgb_to_hex((142,17,26))
    '#8e111a'
    
    >>> rgb_to_hex((142,17,26,14))
    Traceback (most recent call last):
        ...
    AssertionError: le paramètre doit être un tuple de 3 nombres entiers
    
    >>> rgb_to_hex((142,17,600))
    Traceback (most recent call last):
        ...
    AssertionError: les valeurs du triplet doivent être entre 0 et 255
    
    >>> rgb_to_hex((142,17,50.2))
    Traceback (most recent call last):
        ...
    AssertionError: les valeurs du triplet doivent être entières
    
    '''
    assert len(triplet) == 3, "le paramètre doit être un tuple de 3 nombres entiers"
    couleur = '#'
    for valeur in triplet:
        assert type(valeur) == int, "les valeurs du triplet doivent être entières"
        assert valeur <= 255 and valeur >=0, "les valeurs du triplet doivent être entre 0 et 255"
        hexa = hex(valeur)[-2]+hex(valeur)[-1]
        if hexa[0] == 'x':
            hexa = '0' + hexa[1]
        couleur = couleur + hexa
    return couleur
    




doctest.testmod()
    