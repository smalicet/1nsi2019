import doctest

def somme(t):
    '''
    @param t : tuple composé de nombres
    @return : nombre qui est la somme de tous les nombres du tuple
    >>> somme((1, 5, 8, 10))
    24
    >>> somme((2,7,14,28,32))
    83
    >>> somme((1,2,3))
    6
    >>> somme((2,'r'))
    Traceback (most recent call last):
        ...
    AssertionError: le tuple doit être composé de nombre
    '''
    s = 0
    for elt in t:
        assert type(elt) in [float,int], "le tuple doit être composé de nombre"
        s += elt    #la même chose que s = s + elt
    return s

doctest.testmod()