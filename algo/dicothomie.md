```python
import random
def DevinerNombre(N):
    '''
    @param N : un entier
    @return : le nombre de coup nécessaires pour trouver le nombre aléatoire : un entier
    '''
    nbre_tire = random.randint(0,N)
    choix = int(input('A votre avis, quel est mon nombre ? ')) 
    nbCoup = 1
    while choix != nbre_tire :
        if choix > nbre_tire:
            print('mon nombre est plus petit')
            choix = int(input('A votre avis, quel est mon nombre ? ')) 
            nbCoup = nbCoup + 1
        else:
            nbCoup += 1
            print('mon nombre est plus grand')
            choix = int(input('A votre avis, quel est mon nombre ? ')) 
    print('trouvé')
    return nbCoup
```