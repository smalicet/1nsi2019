Les algorithmes gloutons s'efforcent d'apporter la solution optimale au problème posé.     
Les algorithmes des plus proches voisins qu'on appelle aussi ___algorithmes à base d'apprentissage__ apportent une réponse plausible mais pas forcément exacte : on parle d'une prédiction.   
On utilise ce type d'algorithme lorsqu'il est difficile d'appliquer des algorithmes traditionnels.  
  
# Première approche des algorithmes à base d'apprentissage  

## Un alligator et un crocodile c'est caïman pareil ?

Soit la situation suivante :

![Corcodile ou alligator ?](./fig/situation.png)

Pour différencier un crocodile d'un alligator certaines caractéristiques physiques permettent de nous aider :

|![Crocodile](./fig/crocodile.png)|![Alligator](./fig/alligator.png)|
|:---:|:---:|
|Les Crocodiles sont plus grands que les alligators|Les alligators sont plus petits|
|Les Crocodiles ont une gueule courte |Les Alligators ont des geules plus allongées|

Nous aurons donc besoin de données sur ces deux __classes__ d'animaux. Ces données portent sur deux critères:  

- La longueur de leur corps.
- La taille de leur gueule.

## Représentation graphique des données

Les données récoltées ont été analysées par un spécialiste des alligators et crocodiles et il a pu pour chaque couple de données affirmer si l'animal était dans la classe alligator ou dans la classe crocodile.  
Ces données seront appelées données __du jeu d'apprentissage__.   
Chaque animal sera qualifié de __point de données__ et le nom de la classe sera appelé __l'étiquette__ de la classe. 

Pour plus de clarté, ces __points de données__ ont été portés sur un graphique avec en ordonnée la longueur du corps et en abscisse la taille de la gueule.  

![Graphique regroupant les données des crocodiles et aligators](./fig/knn_aligators_analyse.png)


Les points de données bleus représentent les alligators, les points de données rouges les crocodiles.  
Les points de données verts eux représentent des animaux dont on a les caractéristiques mais qui n'ont pas été examinés par le spécialiste : ils ne font pas partis du jeu d'apprentissage.  

Les points de données nous montrent trois zones bien distinstes:  

*  une qui regroupe l'ensemble des crocodiles. (Classe crocodile) 
*  une autre regroupant l'ensemble des alligators. (Classe alligator) 
*  une zone qui est le reste sur laquelle il est difficile de se prononcer

Ici, d'après le graphique, nous pouvons dire que le point de donnée "mystère 1" doit être de la classe alligator, le point de donnée "mystére 2" doit être de la classe crocodile mais il est difficile de se prononcer pour le point de donnée "mystère 3" car il est graphiquement entre les classes alligator et crocodile.  

> 📢 Le but d'un algorithme des plus proches voisins __KNN__ (_k Nearest Neighbors_) est de pouvoir faire une prédiction sur une caractéristique d'un objet en fonction des caratèristiques d'objets similaires déjà connus. 

# Construction d'un algorithme des plus proches voisins.  

Il existe différents types d'algorithmes KNN  nous allons développer dans ce cours est l'__algorithme KNN avec apprentissage supervisé__.  
Cela reprend notre premier exemple : nous disposons des points de données du jeu d'apprentissage qui ont été supervisées par un expert.  L'expert nous permet donc de trouver les classes et de leur donner une étiquette.  

## Comment avons nous réussi à attribuer une classe aux animaux mystères dans l'exemple alligators-crocodiles?  

Nous avons simplement observé graphiquement l'environnement de chaque point de donnée mystère.  
Si par exemple nous prenons les quatre plus proches voisins des points de données mystères : 

* le point de donnée __mystère 1__ est entouré d'animaux de classe alligator. Nous avons donc attribué la classe alligator au point de donnée __mystère 1__.   
* De la même manière pour le point de donnée __mystère 2__, nous lui avons attribué la classe crocodile.   
* Pour le point de donnée __mystère 3__ il nous est impossible de trancher car parmi les quatre voisins il y a autant de points de données de classe crocodile que de points de donnée de classe alligator. (Il faudra donc consulter l'expert) 

![Graphique regroupant les données des crocodiles et aligators.](./fig/knn_aligators_kvoisins.png)  

Même si les résultats obtenus ne sont pas forcément exacts, nous allons nous appuyer sur cette idée de plus proches voisins pour construire nos algorithmes.  

__Nous regarderons la classe des plus proches voisins d'un point de données mystère. Nous déterminerons la classe majoritaire. Cette classe majoritaire sera la classe du point de données mystère.__  

Il nous faudra donc choisir avec __combien de voisins__ nous voulons travailler et calculer __la distance entre le point de donnée mystère et les points de donnée du jeu d'apprentissage__ pour trouver les voisins les plus proches.  

## Comment calculer les distances entre le point de données mystère et les points de donnée du jeu d'apprentissage ?  

Nous utiliserons cette année deux méthodes de calcul de distance. Il est à noter que le résultat de l'algorithme KNN dépend de ce choix.

### La distance Euclidienne

![Distance euclidienne](./fig/euclidienne.png)

> 📢 La distance euclidienne est la distance fréquemment utilisé en mathématique pour mesurer la distance entre deux points d'un graphique : c'est en quelque sorte la 'distance à vol d'oiseau'  
> Dans le cas de deux points de coordonnées  $`(x_{1};y_{1})`$ et $`(x_{2} ; y_{2})`$ la formule est :  $`D_{euclidienne} = \sqrt{(x_{1} - x_{2})^{2} + (y_{1} - y_{2})^{2}}`$

_On reprend l'exemple du I._

* Point de donnée mystère 1 : longueur = 4,2 m  et taille de la gueule = 0,30 m 
* Point de donnée du jeu d'apprentissage: longueur = 4,8 m et taille de la gueule = 0,08 m  

$`D_{euclidienne} = \sqrt{(t_{1} - t_{2})^{2} + (l_{1} - l_{2})^{2}}`$  
$`D_{euclidienne}=\sqrt{(0,30 - 0,08)^{2} +(4,2 - 4,8) ^{2}}
\approx 0,64`$

_Remarque : Si nous avons plus de deux caractéristiques, nous généraliserons la formule ci-dessus._  

### La distance de Manhattan

![Distance de Manhattan](./fig/manhattan.png)
    
> 📢 La distance de Manhattan est la distance entre deux points parcourue par un taxi lorsqu'il se déplace dans une ville où les rues sont agencées selon un réseau ou quadrillage (en utilisant les déplacements horizontaux et verticaux du réseau)  
>   $`D_{Manhattan} =\left | x_{1} - x_{2} \right | + \left | y_{1} - y_{2} \right |`$

_On reprend l'exemple du I._ 

$`D_{Manhattan} =\left | 0,30 - 0,08 \right | + \left | 4,2 - 4,8 \right | =0,82`$


## Combien de voisins faut-il choisir ?  

### Retour sur le problème

Nous allons faire varier k (nombre de voisins) de 1 à 10 puisque le jeu d'apprentissage contient au maximum 10 éléments. Nous verrons pour les points de donnée mystères qui sont leurs voisins.  

| | Cas mystère 1 ||| Cas mystère 2 ||| Cas mystère 3 ||  
|:--------:|:--------:|:--------:|:---------:|:---------:|:---------:|:---------:|:---------:|:---------:|  
| | Alligator | Crocodile || Alligator | Crocodile || Alligator | Crocodile |  
| k = 1 | 1 | 0 || 0 | 1 || 1 | 0 |  
| k = 2 | 2 | 0 || 0 | 2 || 1 | 1 |  
| k = 3 | 3 | 0 || 0 | 3 || 2 | 1 |  
| k = 4 | 4 | 0 || 0 | 4 || 2 | 2 |  
| k = 5 | 5 | 0 || 0 | 5 || 3 | 2 |  
| k = 6 | 5 | 1 || 1 | 5 || 3 | 3 |  
| k = 7 | 5 | 2 || 2 | 5 || 4 | 3 |  
| k = 8 | 5 | 3 || 3 | 5 || 5 | 3 |  
| k = 9 | 5 | 4 || 4 | 5 || 5 | 4 |  
| k = 10 | 5 | 5 || 5 | 5 || 5 | 5 |  

Nous voyons, pour les points de données mystères 1 et 2, que prendre un maximum de voisins n'est pas la meilleure solution car pour k inférieur ou égal à 5 nous pouvions nous prononcer alors que pour k = 10, nous ne pouvons plus. Nous prenons trop de voisins. On ne peut plus donner une prédiction tranchée.  

Cependant, pour k inférieur ou égal à 5, nous ne pouvons pas nous prononcer pour le point de données mystère 3 car ce n'est pas tranché.

Le cas idéal semble être k = 8 car l'écart est significatif pour les trois points de données mystères.  

_Remarque : si nous travaillons avec trop peu de points de données dans le jeu d'apprentissage, dans tous les cas il est difficile de trancher._ 

> 📢 Dans un algorithme KNN, il faut un jeu d'apprentissage important pour avoir des prédictions fiables.

### Comment choisir la valeur de k lorsque le jeu d'aprentissage comporte par exemple 100 éléments ?  

Nous allons diviser la base de données (Alligators Crocodiles) en deux parties:  

- La première partie servira à réaliser la courbe d'apprentissage c'est à dire qu'ils vont représenter les points de données qui vont être utilisés pour mesurer les distances avec les points de donnée mystères. Souvent on prend 75% des points de données. Ils représenteront le __jeu d'apprentissage__.

- La deuxième partie (25%) servira à réaliser des tests. Nous testerons ces 25% sur la courbe d'apprentissage en faisant varier __k__ comme ci-dessus. Comme nous connaissons la classe de ces éléments, nous verrons pour chaque élément si la prédiction de notre algorithme KNN a été juste ou non. Nous pourrons choisir la valeur de __k__ pour laquelle il y a eu le plus de prédictions justes.  

# Ce qu'il faut retenir:  

- Il y a plusieurs types d'algorithme d'apprentissage. Mais nous ne travaillerons qu'avec les algorithmes d'apprentissage supervisé.  

> __Comment réaliser une prédiction sur un point de donnée mystère ?__ 
> 
> * Calculer la distance entre le point de données mystère et les points de données du jeu d'apprentissage. 
> * Fixer le nombre de voisins pour effectuer la prédiction.  
> * Déterminer la classe majoritaire des voisins. Elle fixera la classe du point de donnée mystère.  

* Pour évaluer la fiabilité de notre prédiction, nous divisons les point de données visés par l'expert en deux catégories:  

  - 75% des points de données vont former le jeu d'apprentissage. Ils vont servir à construire la courbe d'apprentissage.  
  - 25% des points de données vont former le jeu de tests. On teste ces points de données sur la courbe d'apprentissage. Comme on connait leur classe, on pourra déterminer le taux de fiabilité de notre prédiction.  
